require "./server/executor"

module Server
  VERSION = "0.1.0"

  Executor.execute ARGV
end
