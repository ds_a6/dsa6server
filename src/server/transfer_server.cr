require "socket"
require "dsa6common"

module Server
  include Common

  class TransferServer
    def initialize(host : String, port : Int)
      @server = TCPServer.new(host, port)
    end

    def run(&block : (Messages::FileRequest, TCPSocket) -> _)
      while client = @server.accept?
        sock = client.as(TCPSocket)
        spawn do
          block.call Messages::FileRequest.from_msgpack(sock), sock
        rescue e : Exception
          sock.send Messages::Response.new(false, e.message.to_s).to_msgpack

          STDERR.puts "ERR: #{e.message}"
        end
      end
    end
  end
end
