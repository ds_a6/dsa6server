require "file_utils"

module Server
  class FileManager
    def initialize(directory : String)
      @dir = directory
    end

    def add_file(filename : String, content : Bytes)
      filename = select_filename filename

      File.write joint_filename(filename), content

      {filename: filename, bytes: content.size}
    end

    private def split_basename(filename : String)
      extname = File.extname filename
      basename = File.basename filename, extname

      [basename, extname]
    end

    private def joint_filename(filename : String)
      File.join(Dir.new(@dir).path, filename)
    end

    private def select_filename(original_filename : String)
      return original_filename unless File.exists? joint_filename(original_filename)

      basename, extname = split_basename(original_filename)

      max_match = 0

      Dir.new(@dir).each do |ent|
        match = /#{basename}__\[([0-9]+)\]/.match split_basename(ent)[0]
        next if match.nil?

        max_match = [match[1].to_i, max_match].max
      end

      "#{basename}__[#{max_match + 1}]#{extname}"
    end
  end
end
