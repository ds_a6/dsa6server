require "./transfer_server"
require "./file_manager"

require "option_parser"

module Server
  class Executor
    def self.execute(args)
      options = parse_args args

      target_dir = File.real_path options[:directory]
      host = options[:host]
      port = options[:port]

      server = TransferServer.new(host, port)
      fman = FileManager.new target_dir

      puts "Running on tcp://#{host}:#{port}"
      puts "Files will be stored in '#{target_dir}'"

      server.run do |message, sock|
        result = fman.add_file(message.filename, message.contents)

        puts "from #{sock.remote_address} uploaded #{result[:bytes]} bytes as '#{result[:filename]}'"
        sock.write Messages::Response.new(true, result[:filename]).to_msgpack
      end
    rescue e : Exception
      STDERR.puts "ERR: #{e.message}"
      exit -1
    end

    private def self.parse_args(args)
      directory = nil
      host = nil
      port = nil

      OptionParser.parse(args) do |parser|
        parser.banner = "Usage: dsa6client [args]"

        parser.on("-d DIR", "--directory=DIR", "directory to upload files to") { |_directory| directory = _directory }
        parser.on("-h HOST", "--host=HOST", "host to listen on") { |_host| host = _host }
        parser.on("-p PORT", "--port=PORT", "port to listen on") { |_port| port = _port.to_i }
        parser.on("-?", "--help", "Display this help message") do
          puts parser
          exit 0
        end
      end

      raise ArgumentError.new("directory not specified") if directory.nil?
      raise ArgumentError.new("host not specified") if host.nil?
      raise ArgumentError.new("port not specified") if port.nil?

      {
        directory: (directory.as String),
        host:      (host.as String),
        port:      (port.as Int32),
      }
    end
  end
end
