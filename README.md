# Distributed Systems - Assignment 6 - File transfer server

## Requirements

* Crystal ~= 0.35.1

## Installation

```bash
git clone https://gitlab.com/ds_a6/dsa6server.git
cd dsa6server
shards --production build --no-debug
```

All dependencies will be resolved automatically. After building, executable will be located in `bin/` folder.

## Usage

Use `dsa6server --help` command to find out usage details.
